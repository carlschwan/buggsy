module github.com/pontaoski/buggsy

go 1.14

require (
	github.com/dgraph-io/badger v1.6.1
	github.com/gomarkdown/markdown v0.0.0-20200316172748-fd1f3374857d
	github.com/jinzhu/gorm v1.9.12
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0
)
