package app

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func List(c echo.Context) error {
	var bugs []Bug
	db.Find(&bugs)
	return c.Render(http.StatusOK, "list", bugs)
}

func View(c echo.Context) error {
	id := c.Param("id")
	intID, _ := strconv.ParseInt(id, 10, 32)
	p := loadBug(int(intID))
	return c.Render(http.StatusOK, "view", p)
}
