package app

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func Save(c echo.Context) error {
	id := c.Param("id")
	intID, _ := strconv.ParseInt(id, 10, 32)
	body := c.FormValue("body")
	title := c.FormValue("title")
	p := &Bug{Title: title, Body: body}
	p.ID = uint(intID)
	p.save()
	return c.Redirect(http.StatusFound, "/view/"+id)
}

func Edit(c echo.Context) error {
	id := c.Param("int")
	intID, _ := strconv.ParseInt(id, 10, 32)
	p := loadBug(int(intID))
	if p.ID == 0 {
		p = Bug{}
		p.ID = 12345
	}
	return c.Render(http.StatusOK, "edit", p)
}
