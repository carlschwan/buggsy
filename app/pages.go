package app

import (
	"github.com/gomarkdown/markdown"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Bug struct {
	gorm.Model
	Title string
	Body  string
}

func (b Bug) Render() string {
	md := []byte(b.Body)
	output := markdown.ToHTML(md, nil, nil)
	return string(output)
}

func (b Bug) save() {
	var bug Bug
	var def Bug
	db.First(&bug, "id = ?", b.ID)
	if bug == def {
		db.Create(&b)
	}
	db.Model(&b).Update("title", b.Title)
	db.Model(&b).Update("body", b.Body)
}

func loadBug(id int) Bug {
	var bug Bug
	db.First(&bug, "id = ?", id)
	return bug
}
